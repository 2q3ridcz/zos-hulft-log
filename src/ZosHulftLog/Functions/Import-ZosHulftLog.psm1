﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path

$DllFilePath = $ProjectFolderPath | Join-Path -ChildPath "CSharp\build\ZosHulftLog.dll"
[System.Reflection.Assembly]::LoadFile($DllFilePath) | Out-Null

<#
.Synopsis
   Reads ZosHulftLog binary file and returns ZosHulftLog objects.
.DESCRIPTION
   Reads ZosHulftLog binary file and returns ZosHulftLog objects.
.EXAMPLE
   Import-ZosHulftLog -Path "C:\ZosHulftLog.txt"
#>
function Import-ZosHulftLog
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipeline=$true,
                   Position=0)]
        [String]
        $Path
    )

    Begin
    {
    }
    Process
    {
        $Bytes = [System.IO.File]::ReadAllBytes($Path)
        [ZosHulftLog.Converter.BytesConverter]::Convert($Bytes)
    }
    End
    {
    }
}