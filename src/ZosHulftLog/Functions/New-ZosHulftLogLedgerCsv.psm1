﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path

$DllFilePath = $ProjectFolderPath | Join-Path -ChildPath "CSharp\build\ZosHulftLog.dll"
[System.Reflection.Assembly]::LoadFile($DllFilePath) | Out-Null

<#
.Synopsis
   Reads ZosHulftLog binary file and writes ZosHulftLogLedger file.
.DESCRIPTION
   Reads ZosHulftLog binary file and writes ZosHulftLogLedger file.
.EXAMPLE
   New-ZosHulftLogLedgerCsv -HulftLogPath "C:\ZosHulftLog.txt" -OutCsvPath "C:\ZosHulftLog.csv"
#>
function New-ZosHulftLogLedgerCsv
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipeline=$true,
                   Position=0)]
        [String]
        $HulftLogPath
        ,
        [Parameter(Mandatory=$true,
                   Position=1)]
        [string]
        $OutCsvPath
        ,
        [Parameter(Mandatory=$false,
                   Position=2)]
        [string]
        $Encoding = "Default"
    )

    Begin
    {
    }
    Process
    {
        $Bytes = [System.IO.File]::ReadAllBytes($HulftLogPath)
        $Records = @(
            [ZosHulftLog.Converter.BytesConverter]::Convert($Bytes) |
            ?{ @("R", "S") -contains $_.LogClassification } |
            ?{ $_.LogClassificationDefs.Count -Eq 1 }
        )

        $Header = [ZosHulftLog.Presenter.SendReceiveLogLedgerRecord]::Header        

        @(
            $Header -join "," | Write-Output

            [ZosHulftLog.Presenter.SendReceiveLogLedger]::Convert($Records) |
            %{
                $Obj = $_
                @($Header | %{ $Obj.$_ }) -join ","
            } | Write-Output
        ) |
        Out-File -FilePath $OutCsvPath -Encoding $Encoding
    }
    End
    {
    }
}
