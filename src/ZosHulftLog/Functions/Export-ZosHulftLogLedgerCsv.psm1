﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path

$DllFilePath = $ProjectFolderPath | Join-Path -ChildPath "CSharp\build\ZosHulftLog.dll"
[System.Reflection.Assembly]::LoadFile($DllFilePath) | Out-Null

<#
.Synopsis
   Converts ZosHulftLog send log and receive log object to ZosHulftLogLedger object and output csv file.
.DESCRIPTION
   Converts ZosHulftLog send log and receive log object to ZosHulftLogLedger object and output csv file.
.EXAMPLE
   Export-ZosHulftLogLedgerCsv -InputObject $Logs -Path "C:\log.csv"
#>
function Export-ZosHulftLogLedgerCsv
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipeline=$true,
                   Position=0)]
        [Object]
        $InputObject
        ,
        [Parameter(Mandatory=$true,
                   Position=1)]
        [string]
        $Path
        ,
        [Parameter(Mandatory=$false,
                   Position=2)]
        [string]
        $Encoding = "Default"
    )

    Begin
    {
        $TempList = @()
    }
    Process
    {
        $TempList += $InputObject
    }
    End
    {
        $Header = [ZosHulftLog.Presenter.SendReceiveLogLedgerRecord]::Header
        
        @(
            $Header -join "," | Write-Output

            [ZosHulftLog.Presenter.SendReceiveLogLedger]::Convert($TempList) |
            %{
                $Obj = $_
                @($Header | %{ $Obj.$_ }) -join ","
            } | Write-Output
        ) |
        Out-File -FilePath $Path -Encoding $Encoding
    }
}