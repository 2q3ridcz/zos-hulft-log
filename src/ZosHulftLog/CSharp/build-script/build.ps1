$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path

Push-Location -Path $ProjectFolderPath

$DotNetFwFolderPath = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319"

# $CscPath = $DotNetFwFolderPath | Join-Path -ChildPath "csc.exe"
# .($CscPath) $SourceFilePath ("-out:" + $OutputFilePath) "-target:library"

$MsBuildPath = $DotNetFwFolderPath | Join-Path -ChildPath "MSBuild.exe"
.($MsBuildPath)

Pop-Location
