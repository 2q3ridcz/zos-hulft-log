using System;
using System.Collections.Generic;

namespace ZosHulftLog
{
    namespace Presenter
    {
        public class SendReceiveLogLedger
        {
            public static SendReceiveLogLedgerRecord[] Convert(SendReceiveLogRecord[] hulftLogRecords)
            {
                List<SendReceiveLogLedgerRecord> records = new List<SendReceiveLogLedgerRecord>();

                foreach(SendReceiveLogRecord log in hulftLogRecords) {
                    string logType = log.LogClassification;
                    Int64 date = System.Convert.ToInt64(log.StartDateTime.ToString("yyyyMMdd"));
                    Int64 startTime = System.Convert.ToInt64(log.StartDateTime.ToString("HHmmssff"));
                    Int64 endTime = System.Convert.ToInt64(log.EndDateTime.ToString("HHmmssff"));
                    string transFileId = log.FileID;
                    string host = log.HostName;
                    string filePath = log.DatasetName;
                    Int64 recCnt = log.RecordCount;
                    Int64 dataSize = log.DataSize;

                    SendReceiveLogLedgerRecord record = new SendReceiveLogLedgerRecord(
                        logType,
                        date,
                        startTime,
                        endTime,
                        transFileId,
                        host,
                        filePath,
                        recCnt,
                        dataSize
                    );
                    
                    records.Add(record);
                }
                return records.ToArray();
            }
        }
    }
}
