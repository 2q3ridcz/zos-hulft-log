using System;
using System.Collections.Generic;

namespace ZosHulftLog
{
    namespace Presenter
    {
        public class SendReceiveLogLedgerRecord
        {
            public static List<string> Header
            {
                get { return new List<string> { "LogType", "Date", "StartTime", "EndTime", "TransFileId", "Host", "FilePath", "RecCnt", "DataSize" }; }
            }

            public SendReceiveLogLedgerRecord(
                string LogType,
                Int64 Date,
                Int64 StartTime,
                Int64 EndTime,
                string TransFileId,
                string Host,
                string FilePath,
                Int64 RecCnt,
                Int64 DataSize
            )
            {
                this.LogType = LogType;
                this.Date = Date;
                this.StartTime = StartTime;
                this.EndTime = EndTime;
                this.TransFileId = TransFileId;
                this.Host = Host;
                this.FilePath = FilePath;
                this.RecCnt = RecCnt;
                this.DataSize = DataSize;
            }
            
            private string _LogType;
            public string LogType
            {
                get { return _LogType; }
                set { _LogType = value; }
            }
            
            private Int64 _Date;
            public Int64 Date
            {
                get { return _Date; }
                set { _Date = value; }
            }
            
            private Int64 _StartTime;
            public Int64 StartTime
            {
                get { return _StartTime; }
                set { _StartTime = value; }
            }

            private Int64 _EndTime;
            public Int64 EndTime
            {
                get { return _EndTime; }
                set { _EndTime = value; }
            }

            private string _TransFileId;
            public string TransFileId
            {
                get { return _TransFileId; }
                set { _TransFileId = value; }
            }

            private string _Host;
            public string Host
            {
                get { return _Host; }
                set { _Host = value; }
            }

            private string _FilePath;
            public string FilePath
            {
                get { return _FilePath; }
                set { _FilePath = value; }
            }

            private Int64 _RecCnt;
            public Int64 RecCnt
            {
                get { return _RecCnt; }
                set { _RecCnt = value; }
            }

            private Int64 _DataSize;
            public Int64 DataSize
            {
                get { return _DataSize; }
                set { _DataSize = value; }
            }
        }
    }
}
