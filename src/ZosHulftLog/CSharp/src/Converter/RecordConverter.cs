using System;
using System.Collections.Generic;  
using System.Linq;

namespace ZosHulftLog
{
    namespace Converter
    {
        public class RecordConverter
        {
            public static HulftLogRecord Convert(byte[] bytes, Int64 position)
            {
                if (bytes.Length <= position) {
                    return null;
                }
                // 本当は各レコードの知識をこのクラスに入れたくない…
                string logClassification = bytes[position + 0].ToString("X2");
                string secondByte = bytes[position + 1].ToString("X2");

                if (logClassification == "D9") { // R
                    if (secondByte == "00" || secondByte == "FF") {
                        // return Convert(bytes, position + 191);
                        DelimiterRecordConverter converter = new DelimiterRecordConverter();
                        return converter.Convert(bytes, position);
                    } else {
                        ReceiveLogRecordConverter converter = new ReceiveLogRecordConverter();
                        return converter.Convert(bytes, position);
                    }
                }
                
                if (logClassification == "E2") { // S
                    if (secondByte == "00" || secondByte == "FF") {
                        // return Convert(bytes, position + 191);
                        DelimiterRecordConverter converter = new DelimiterRecordConverter();
                        return converter.Convert(bytes, position);
                    } else {
                        SendLogRecordConverter converter = new SendLogRecordConverter();
                        return converter.Convert(bytes, position);
                    }
                }
                
                if (logClassification == "D1") { // J
                    // return Convert(bytes, position + 393);
                    JobExecutionLogRecordConverter converter = new JobExecutionLogRecordConverter();
                    return converter.Convert(bytes, position);
                }

                if (logClassification == "C3") { // C
                    if (secondByte == "00" || secondByte == "FF") {
                        // return Convert(bytes, position + 191);
                        DelimiterRecordConverter converter = new DelimiterRecordConverter();
                        return converter.Convert(bytes, position);
                    } else {
                        // return Convert(bytes, position + 526);
                        ObserveLogRecordConverter converter = new ObserveLogRecordConverter();
                        return converter.Convert(bytes, position);
                    }
                }
                
                if (logClassification == "C1") { // A
                    // return Convert(bytes, position + 169);
                    EmptyLogRecordConverter converter = new EmptyLogRecordConverter();
                    return converter.Convert(bytes, position);
                }

                string bytes_string = ByteArray.ToEBCIDICString(bytes, position, 40);
                throw new Exception("Invalid bytes (at position " + position.ToString() + "): " + bytes_string);
            }
            public static HulftLogRecord Convert(byte[] bytes)
            {
                return Convert(bytes, 0);
            }
        }
    }
}
