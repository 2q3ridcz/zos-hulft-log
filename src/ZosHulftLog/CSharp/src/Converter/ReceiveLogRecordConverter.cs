using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    namespace Converter
    {
        public class ReceiveLogRecordConverter
        {
            public ReceiveLogRecordConverter(){}

            public ReceiveLogRecord Convert(byte[] bytes, Int64 position)
            {
                string logClassification = ByteArray.ToEBCIDICString(bytes, position + 0, 1);
                DateTime registerationDateTime = ByteArray.ToDateTime(bytes, position + 51);
                DateTime registerationDateTimeSK2 = ByteArray.ToDateTime(bytes, position + 110);
                DateTime registerationDateTimeSK3 = ByteArray.ToDateTime(bytes, position + 119);
                string fileID = ByteArray.ToEBCIDICString(bytes, position + 1193, 50);
                string hostName = ByteArray.ToEBCIDICString(bytes, position + 135, 50);
                string datasetName = ByteArray.ToEBCIDICString(bytes, position + 210, 44);

                // 本当は RecordCount2 300 P 10を設定したい。
                // Int64 recordCount = ByteArray.ToInt64(bytes, position + 206, 4);
                Int64 recordCount = ByteArray.PackedBCDToInt64(bytes, position + 300, 10);

                // 本当は DataSize2 310 P 10を設定したい。
                // Int64 dataSize = ByteArray.ToInt64(bytes, position + 260, 4);
                Int64 dataSize = ByteArray.PackedBCDToInt64(bytes, position + 310, 10);

                // ReceiveStartDate 185 P 5	Receive start date (YYYYMMDD)	
                // ReceiveStartTime 190 P 5	Receive start time (HHMMSSmm)	
                // ReceiveEndTime 195 P	5 Receive end time (HHMMSSmm)	
                // ReceiveEndDate 330 P	5 Receive end date (YYYYMMDD)	
                DateTime startDateTime = ByteArray.PackedBCDToDatetime(bytes, position + 185);
                DateTime endDateTime = ByteArray.PackedBCDToDatetime(bytes, position + 330, position + 195);

                
                ReceiveLogRecord record = new ReceiveLogRecord(
                    logClassification,
                    registerationDateTime,
                    registerationDateTimeSK2,
                    registerationDateTimeSK3,
                    fileID,
                    hostName,
                    datasetName,
                    startDateTime,
                    endDateTime,
                    dataSize,
                    recordCount
                );
                return record;
            }
            public ReceiveLogRecord Convert(byte[] bytes)
            {
                return Convert(bytes, 0);
            }
        }
    }
}
