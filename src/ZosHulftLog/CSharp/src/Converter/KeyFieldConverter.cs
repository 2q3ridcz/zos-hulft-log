using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    namespace Converter
    {
        public class KeyFieldConverter
        {
            public KeyFieldConverter(){}

            public KeyField Convert(byte[] bytes)
            {
                string logClassification = ByteArray.ToEBCIDICString(bytes, 0, 1);
                string fileID = ByteArray.ToEBCIDICString(bytes, 1, 50);
                string hostName = ByteArray.ToEBCIDICString(bytes, 60, 50);
                DateTime registerationDateTime = ByteArray.ToDateTime(bytes, 51);
                DateTime registerationDateTimeSK2 = ByteArray.ToDateTime(bytes, 110);
                DateTime registerationDateTimeSK3 = ByteArray.ToDateTime(bytes, 119);
                
                KeyField keyField = new KeyField(
                    logClassification,
                    fileID,
                    hostName,
                    registerationDateTime,
                    registerationDateTimeSK2,
                    registerationDateTimeSK3
                );
                return keyField;
            }
        }
    }
}
