using System;
using System.Collections.Generic;
using System.Linq;

namespace ZosHulftLog
{
    namespace Converter
    {
        public class ByteArray
        {
            public static byte[] SubBytes(byte[] bytes, Int64 start, Int64 length)
            {
                byte[] subBytes = new byte[length];
                Array.Copy(bytes, start, subBytes, 0, length);
                return subBytes;
            }

            public static string ToEBCIDICString(byte[] bytes)
            {
                EBCDIC.EBCDICEncoding encoding = new EBCDIC.EBCDICEncoding();
                return encoding.GetString(bytes, " ").Trim();
            }
            public static string ToEBCIDICString(byte[] bytes, Int64 start, Int64 length)
            {
                return ToEBCIDICString(SubBytes(bytes, start, length));
            }

            public static Int64 PackedBCDToInt64(byte[] bytes, Int64 start, Int64 length)
            {
                string tempString = "";
                for(Int64 i = start; i < start + length; i++)
                {
                    tempString += bytes[i].ToString("X2");
                }

                string signString = tempString.Substring(tempString.Length - 1, 1);
                Int64 sign = 1;
                string[] positiveLetters = { "C", "F" };
                if (signString == "D") {
                    sign = -1;
                } else if (positiveLetters.Contains(signString)) {
                    sign = 1;
                } else {
                    throw new Exception("Is not a packedBCD(at position " + start.ToString() + "): '" + tempString + "'");
                }

                Int64 num; 
                string numString = tempString.Substring(0, tempString.Length - 1);
                bool parsed = Int64.TryParse(numString, out num);

                if (parsed) {
                    return sign * num;
                } else {
                    throw new Exception("Is not a packedBCD(at position " + start.ToString() + "): '" + tempString + "'");
                }
            }
            public static Int64 PackedBCDToInt64(byte[] bytes)
            {
                return PackedBCDToInt64(bytes, 0, bytes.Length);
            }

            public static Int64 ToInt64(byte[] bytes, Int64 start, Int64 length)
            {
                string tempString = "";
                for(Int64 i = start; i < start + length; i++)
                    tempString += bytes[i].ToString("X2");
                return Int64.Parse(
                    tempString,
                    System.Globalization.NumberStyles.AllowHexSpecifier
                );
            }
            public static Int64 ToInt64(byte[] bytes)
            {
                return ToInt64(bytes, 0, bytes.Length);
            }

            public static DateTime ToDateTime(byte[] bytes, Int64 start)
            {
                int byteLength = 8;

                string tempString = "";
                for(Int64 i = start; i < start + byteLength; i++)
                    tempString += bytes[i].ToString("X2");
                if (tempString == "0000000000000000") {
                    return DateTime.MinValue;
                }
                if (tempString == "FFFFFFFFFFFFFFFF") {
                    return DateTime.MaxValue;
                }

                Int64 date_int = ToInt64(bytes, start, 4);
                string date_string = date_int.ToString("00000000");

                Int64 time_int = ToInt64(bytes, start + 4, 4);
                string time_string = time_int.ToString("00000000");
                
                return DateTime.ParseExact(
                    date_string + time_string,
                    "yyyyMMddHHmmssff",
                    null
                );
            }
            public static DateTime ToDateTime(byte[] bytes)
            {
                return ToDateTime(bytes, 0);
            }

            public static DateTime PackedBCDToDatetime(byte[] bytes, Int64 date_position, Int64 time_position)
            {
                Int64 date_int = PackedBCDToInt64(bytes, date_position, 5);
                string date_string = date_int.ToString("00000000");

                Int64 time_int = PackedBCDToInt64(bytes, time_position, 5);
                string time_string = time_int.ToString("00000000");
                
                return DateTime.ParseExact(
                    date_string + time_string,
                    "yyyyMMddHHmmssff",
                    null
                );
            }
            public static DateTime PackedBCDToDatetime(byte[] bytes, Int64 datetime_position)
            {
                return PackedBCDToDatetime(bytes, datetime_position, datetime_position + 5);
            }
            public static DateTime PackedBCDToDatetime(byte[] bytes)
            {
                return PackedBCDToDatetime(bytes, 0, 5);
            }
        }
    }
}
