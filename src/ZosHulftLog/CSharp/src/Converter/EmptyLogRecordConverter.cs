using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    namespace Converter
    {
        public class EmptyLogRecordConverter
        {
            public EmptyLogRecordConverter(){}

            public EmptyLogRecord Convert(byte[] bytes, Int64 position)
            {
                string logClassification = ByteArray.ToEBCIDICString(bytes, position + 0, 1);
                DateTime registerationDateTime = ByteArray.ToDateTime(bytes, position + 51);
                DateTime registerationDateTimeSK2 = ByteArray.ToDateTime(bytes, position + 110);
                DateTime registerationDateTimeSK3 = ByteArray.ToDateTime(bytes, position + 119);
                
                EmptyLogRecord record = new EmptyLogRecord(
                    logClassification,
                    registerationDateTime,
                    registerationDateTimeSK2,
                    registerationDateTimeSK3
                );
                return record;
            }
            public EmptyLogRecord Convert(byte[] bytes)
            {
                return Convert(bytes, 0);
            }
        }
    }
}
