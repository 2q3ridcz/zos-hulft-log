using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    namespace Converter
    {
        public class DelimiterRecordConverter
        {
            public DelimiterRecordConverter(){}

            public DelimiterRecord Convert(byte[] bytes, Int64 positon)
            {
                string logClassification = ByteArray.ToEBCIDICString(bytes, positon + 0, 1);
                DateTime registerationDateTime = ByteArray.ToDateTime(bytes, positon + 51);
                DateTime registerationDateTimeSK2 = ByteArray.ToDateTime(bytes, positon + 110);
                DateTime registerationDateTimeSK3 = ByteArray.ToDateTime(bytes, positon + 119);
                
                DelimiterRecord record = new DelimiterRecord(
                    logClassification,
                    registerationDateTime,
                    registerationDateTimeSK2,
                    registerationDateTimeSK3
                );
                return record;
            }
            public DelimiterRecord Convert(byte[] bytes)
            {
                return Convert(bytes, 0);
            }
        }
    }
}
