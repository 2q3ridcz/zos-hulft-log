using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    namespace Converter
    {
        public class SendLogRecordConverter
        {
            public SendLogRecordConverter(){}

            public SendLogRecord Convert(byte[] bytes, Int64 position)
            {
                string logClassification = ByteArray.ToEBCIDICString(bytes, position + 0, 1);
                DateTime registerationDateTime = ByteArray.ToDateTime(bytes, position + 51);
                DateTime registerationDateTimeSK2 = ByteArray.ToDateTime(bytes, position + 110);
                DateTime registerationDateTimeSK3 = ByteArray.ToDateTime(bytes, position + 119);
                string fileID = ByteArray.ToEBCIDICString(bytes, position + 1268, 50);
                string hostName = ByteArray.ToEBCIDICString(bytes, position + 135, 50);
                string datasetName = ByteArray.ToEBCIDICString(bytes, position + 210, 44);
                Int64 recordCount = ByteArray.PackedBCDToInt64(bytes, position + 326, 10);
                Int64 dataSize = ByteArray.PackedBCDToInt64(bytes, position + 346, 10);
                DateTime startDateTime = ByteArray.PackedBCDToDatetime(bytes, position + 185);
                DateTime endDateTime = ByteArray.PackedBCDToDatetime(bytes, position + 366, position + 195);
                
                SendLogRecord record = new SendLogRecord(
                    logClassification,
                    registerationDateTime,
                    registerationDateTimeSK2,
                    registerationDateTimeSK3,
                    fileID,
                    hostName,
                    datasetName,
                    startDateTime,
                    endDateTime,
                    dataSize,
                    recordCount
                );
                return record;
            }
            public SendLogRecord Convert(byte[] bytes)
            {
                return Convert(bytes, 0);
            }
        }
    }
}
