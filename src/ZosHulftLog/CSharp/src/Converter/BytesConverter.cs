using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    namespace Converter
    {
        public class BytesConverter
        {
            public static HulftLogRecord[] Convert(byte[] bytes)
            {
                List<HulftLogRecord> records = new List<HulftLogRecord>();

                Int64 position = 0;

                while (position < bytes.Length)
                {
                    HulftLogRecord record = RecordConverter.Convert(bytes, position);
                    position += record.ByteCount;
                    records.Add(record);
                }

                return records.ToArray();
            }
        }
    }
}
