using System;
using System.Collections.Generic;  

namespace EBCDIC
{
    public class EBCDICEncoding
    {
        // Select code from this page.
        // https://docs.microsoft.com/en-us/dotnet/api/system.text.encoding
        public string Name = "IBM290";

        public EBCDICEncoding(){}

        public byte[] GetBytes(string s)
        {
            return System.Text.Encoding.GetEncoding(this.Name).GetBytes(s);
        }

        public string GetString(byte[] bytes)
        {
            return this.GetString(bytes, "");
        }
        
        public string GetString(byte[] bytes, string controlCharMask)
        {
            string sentence = System.Text.Encoding.GetEncoding(this.Name).GetString(bytes);
            if ( controlCharMask == "" )
                return sentence;

            string replaced = "";
            for (int i = 0; i < sentence.Length; i++)
            {
                if (Char.IsControl(sentence, i))
                {
                    replaced += controlCharMask;
                }
                else
                {
                    replaced += sentence[i];
                }
            }
            return replaced;
        }
    }
}