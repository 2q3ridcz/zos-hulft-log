using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    public class DelimiterRecord : HulftLogRecord
    {
        public override Int64 ByteCount
        {
            get { return 191; }
        }

        public override List<string> LogClassificationDefs
        {
            get { return new List<string> { "C", "R", "S" }; }
        }

        public DelimiterRecord(
            string LogClassification,
            DateTime RegisterationDateTime,
            DateTime RegisterationDateTimeSK2,
            DateTime RegisterationDateTimeSK3
        )
        {
            this.LogClassification = LogClassification;
            this.RegisterationDateTime = RegisterationDateTime;
            this.RegisterationDateTimeSK2 = RegisterationDateTimeSK2;
            this.RegisterationDateTimeSK3 = RegisterationDateTimeSK3;
        }
    }
}
