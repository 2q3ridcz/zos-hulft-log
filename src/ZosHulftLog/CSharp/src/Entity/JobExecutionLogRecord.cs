using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    public class JobExecutionLogRecord : HulftLogRecord
    {
        public override Int64 ByteCount
        {
            get { return 393; }
        }

        public override List<string> LogClassificationDefs
        {
            get { return new List<string> { "J" }; }
        }

        public JobExecutionLogRecord(
            string LogClassification,
            DateTime RegisterationDateTime,
            DateTime RegisterationDateTimeSK2,
            DateTime RegisterationDateTimeSK3
        )
        {
            this.LogClassification = LogClassification;
            this.RegisterationDateTime = RegisterationDateTime;
            this.RegisterationDateTimeSK2 = RegisterationDateTimeSK2;
            this.RegisterationDateTimeSK3 = RegisterationDateTimeSK3;
        }
    }
}
