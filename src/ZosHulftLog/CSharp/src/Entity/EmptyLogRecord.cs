using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    public class EmptyLogRecord : HulftLogRecord
    {
        public override Int64 ByteCount
        {
            get { return 169; }
        }

        public override List<string> LogClassificationDefs
        {
            get { return new List<string> { "A" }; }
        }

        public EmptyLogRecord(
            string LogClassification,
            DateTime RegisterationDateTime,
            DateTime RegisterationDateTimeSK2,
            DateTime RegisterationDateTimeSK3
        )
        {
            this.LogClassification = LogClassification;
            this.RegisterationDateTime = RegisterationDateTime;
            this.RegisterationDateTimeSK2 = RegisterationDateTimeSK2;
            this.RegisterationDateTimeSK3 = RegisterationDateTimeSK3;
        }
    }
}
