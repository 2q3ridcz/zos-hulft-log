using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    public class ReceiveLogRecord : SendReceiveLogRecord
    {
        public override Int64 ByteCount
        {
            get { return 3652; }
        }

        public override List<string> LogClassificationDefs
        {
            get { return new List<string> { "R" }; }
        }

        public ReceiveLogRecord(
            string LogClassification,
            DateTime RegisterationDateTime,
            DateTime RegisterationDateTimeSK2,
            DateTime RegisterationDateTimeSK3,
            string FileID,
            string HostName,
            string DatasetName,
            DateTime StartDateTime,
            DateTime EndDateTime,
            Int64 DataSize,
            Int64 RecordCount
        )
        {
            this.LogClassification = LogClassification;
            this.RegisterationDateTime = RegisterationDateTime;
            this.RegisterationDateTimeSK2 = RegisterationDateTimeSK2;
            this.RegisterationDateTimeSK3 = RegisterationDateTimeSK3;
            this.FileID = FileID;
            this.HostName = HostName;
            this.DatasetName = DatasetName;
            this.StartDateTime = StartDateTime;
            this.EndDateTime = EndDateTime;
            this.DataSize = DataSize;
            this.RecordCount = RecordCount;
        }
    }
}
