using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    public class ObserveLogRecord : HulftLogRecord
    {
        public override Int64 ByteCount
        {
            get { return 526; }
        }

        public override List<string> LogClassificationDefs
        {
            get { return new List<string> { "C" }; }
        }

        public ObserveLogRecord(
            string LogClassification,
            DateTime RegisterationDateTime,
            DateTime RegisterationDateTimeSK2,
            DateTime RegisterationDateTimeSK3
        )
        {
            this.LogClassification = LogClassification;
            this.RegisterationDateTime = RegisterationDateTime;
            this.RegisterationDateTimeSK2 = RegisterationDateTimeSK2;
            this.RegisterationDateTimeSK3 = RegisterationDateTimeSK3;
        }
    }
}
