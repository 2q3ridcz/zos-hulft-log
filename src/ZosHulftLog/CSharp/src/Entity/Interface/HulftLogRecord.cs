using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    public abstract class HulftLogRecord
    {
        public abstract Int64 ByteCount
        {
            get;
        }

        public abstract List<string> LogClassificationDefs
        {
            get;
        }

        private string _LogClassification;
        public string LogClassification
        {
            get { return _LogClassification; }
            set {
                if (this.LogClassificationDefs.IndexOf(value) != -1)
                {
                    _LogClassification = value;
                }
                else
                {
                    throw new Exception("Invalid LogClassification: " + value);
                }
            }
        }
        
        private DateTime _RegisterationDateTime;
        public DateTime RegisterationDateTime
        {
            get { return _RegisterationDateTime; }
            set { _RegisterationDateTime = value; }
        }
        
        private DateTime _RegisterationDateTimeSK2;
        public DateTime RegisterationDateTimeSK2
        {
            get { return _RegisterationDateTimeSK2; }
            set { _RegisterationDateTimeSK2 = value; }
        }
        
        private DateTime _RegisterationDateTimeSK3;
        public DateTime RegisterationDateTimeSK3
        {
            get { return _RegisterationDateTimeSK3; }
            set { _RegisterationDateTimeSK3 = value; }
        }
    }
}
