using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    public abstract class SendReceiveLogRecord : HulftLogRecord
    {
        private string _FileID;
        public string FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }
        
        private string _HostName;
        public string HostName
        {
            get { return _HostName; }
            set { _HostName = value; }
        }
        
        private string _DatasetName;
        public string DatasetName
        {
            get { return _DatasetName; }
            set { _DatasetName = value; }
        }

        private DateTime _StartDateTime;
        public DateTime StartDateTime
        {
            get { return _StartDateTime; }
            set { _StartDateTime = value; }
        }

        private DateTime _EndDateTime;
        public DateTime EndDateTime
        {
            get { return _EndDateTime; }
            set { _EndDateTime = value; }
        }

        private Int64 _DataSize;
        public Int64 DataSize
        {
            get { return _DataSize; }
            set { _DataSize = value; }
        }

        private Int64 _RecordCount;
        public Int64 RecordCount
        {
            get { return _RecordCount; }
            set { _RecordCount = value; }
        }
    }
}
