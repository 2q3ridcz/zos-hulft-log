using System;
using System.Collections.Generic;  

namespace ZosHulftLog
{
    public class KeyField : HulftLogRecord
    {
        public override Int64 ByteCount
        {
            get { return 3652; }
        }

        public override List<string> LogClassificationDefs
        {
            get { return new List<string> { "A", "C", "J", "R", "S" }; }
        }

        public KeyField(
            string LogClassification,
            string FileID,
            string HostName,
            DateTime RegisterationDateTime,
            DateTime RegisterationDateTimeSK2,
            DateTime RegisterationDateTimeSK3
        )
        {
            this.LogClassification = LogClassification;
            this.FileID = FileID;
            this.HostName = HostName;
            this.RegisterationDateTime = RegisterationDateTime;
            this.RegisterationDateTimeSK2 = RegisterationDateTimeSK2;
            this.RegisterationDateTimeSK3 = RegisterationDateTimeSK3;
        }
        
        private string _FileID;
        public string FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }
        
        private string _HostName;
        public string HostName
        {
            get { return _HostName; }
            set { _HostName = value; }
        }
    }
}
