$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path

$PackageName = "ZosHulftLog"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
Import-Module -Name $PackagePath -Force

$InputFolderPath = $here | Join-Path -ChildPath "Input"
$OutputFolderPath = $here | Join-Path -ChildPath "Output"

$InputFolderPath | Join-Path -ChildPath "*.txt" |
Get-ChildItem |
%{
    $File = $_
    $OutFilePath = $OutputFolderPath | Join-Path -ChildPath ($File.BaseName + ".csv")
    Write-Host -Object ($File.Name)
    # $Bytes = [System.IO.File]::ReadAllBytes($File.FullName)
    
    # $Records =
    # [ZosHulftLog.Converter.BytesConverter]::Convert($Bytes) |
    # ?{ @("R", "S") -contains $_.LogClassification } |
    # ?{ $_.LogClassificationDefs.Count -Eq 1 }
    
    # [ZosHulftLog.Presenter.SendReceiveLogLedger]::Convert($Records) |
    # Select-Object -Property @("LogType", "Date", "StartTime", "EndTime", "TransFileId", "Host", "FilePath", "RecCnt", "DataSize") |
    # Export-Csv -Path $OutFilePath -NoTypeInformation -Encoding Default
    New-ZosHulftLogLedgerCsv -HulftLogPath $File.FullName -OutCsvPath $OutFilePath
}

Read-Host -Prompt ("Done! Press enter to exit...")
