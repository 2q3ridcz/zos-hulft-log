# zos-hulft-log

## console_logfile-to-csv

Reads log file of HULFT8 for zOS and outputs receive and send logs to csv file.


## Usage

1. Put zos hulft log files to Input folder.
2. Run console_logfile-to-csv.ps1.
3. The csv file should be in the Output folder.
