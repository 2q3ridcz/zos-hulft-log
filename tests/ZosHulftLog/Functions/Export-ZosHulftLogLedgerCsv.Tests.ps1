﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")

Describe "Export-ZosHulftLogLedgerCsv" {
    Context "Unit test" {
        It "Creates csv when a receive log is given" {
            $Logs = @(
                [ZosHulftLog.ReceiveLogRecord]::new(
                    "R",
                    [datetime]::Now,
                    [datetime]::Now,
                    [datetime]::Now,
                    "ABCDE",
                    "SERVER",
                    "C.TEMP.FILE",
                    [datetime]::ParseExact("2021/12/09 12:41:12.12", "yyyy/MM/dd HH:mm:ss.ff", $null),
                    [datetime]::ParseExact("2021/12/09 12:43:21.76", "yyyy/MM/dd HH:mm:ss.ff", $null),
                    1234567890,
                    42565
                )
            )

            $TestPath = Join-Path $TestDrive -ChildPath 'Creates-csv-when-receive-log-is-given.csv'
            Export-ZosHulftLogLedgerCsv -InputObject $Logs -Path $TestPath

            $Acctual = Get-Content -Path $TestPath -Encoding Default

            $Acctual[0] | Should Be "LogType,Date,StartTime,EndTime,TransFileId,Host,FilePath,RecCnt,DataSize"
            $Acctual[1] | Should Be "R,20211209,12411212,12432176,ABCDE,SERVER,C.TEMP.FILE,42565,1234567890"
        }
    }
}

Describe "Export-ZosHulftLogLedgerCsv Input from pipeline" {
    Context "Unit test" {
        It "Creates csv when a receive log is given" {
            $Logs = @(
                [ZosHulftLog.ReceiveLogRecord]::new(
                    "R",
                    [datetime]::Now,
                    [datetime]::Now,
                    [datetime]::Now,
                    "ABCDE",
                    "SERVER",
                    "C.TEMP.FILE",
                    [datetime]::ParseExact("2021/12/09 12:41:12.12", "yyyy/MM/dd HH:mm:ss.ff", $null),
                    [datetime]::ParseExact("2021/12/09 12:43:21.76", "yyyy/MM/dd HH:mm:ss.ff", $null),
                    1234567890,
                    42565
                )
            )

            $TestPath = Join-Path $TestDrive -ChildPath 'Creates-csv-when-receive-log-is-given.csv'
            $Logs | Export-ZosHulftLogLedgerCsv -Path $TestPath

            $Acctual = Get-Content -Path $TestPath -Encoding Default

            $Acctual[0] | Should Be "LogType,Date,StartTime,EndTime,TransFileId,Host,FilePath,RecCnt,DataSize"
            $Acctual[1] | Should Be "R,20211209,12411212,12432176,ABCDE,SERVER,C.TEMP.FILE,42565,1234567890"
        }
    }
}
