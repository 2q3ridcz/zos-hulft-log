﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")

Describe "Import-ZosHulftLog" {
    Context "Unit test" {
        It "Returns EmptyLogRecord when A-start bytes are given" {
            $Hex = @("C1")
            0..167 | %{ $Hex += "00" }

            $Bytes = $Hex | %{ [Convert]::ToByte($_, 16) }
            $TestPath = Join-Path $TestDrive -ChildPath 'Returns-EmptyLogRecord-when-A-start-bytes-are-given.txt'
            [System.IO.File]::WriteAllBytes($TestPath, $Bytes)

            $Acctual = Import-ZosHulftLog -Path $TestPath

            $Expect = "ZosHulftLog.EmptyLogRecord"
            $Acctual | Should BeOfType $Expect
        }

        It "Returns several types of log records" {
            $AHRec = "C1" + "00" * 168
            $CHRec = "C3" + "00" * 190
            $CDRec = "C340" + "00" * 524
            $CTRec = "C3" + "FF" * 190
            $JDRec = "D140" + "00" * 391
            $RHRec = "D9" + "00" * 190
            # $RDRec = "D940" + "00" * 3650
            $RTRec = "D9" + "FF" * 190
            $SHRec = "E2" + "00" * 190
            # $SDRec = "E240" + "00" * 3791
            $STRec = "E2" + "FF" * 190

            $Hex = @(
                $AHRec,$CHRec,$CDRec,$CTRec,$JDRec,$RHRec,$RTRec,$SHRec,$STRec
            ) -join ""
            $Bytes = 0..(($Hex.Length/2)-1) | %{$Hex.Substring($_*2,2)} | %{ [Convert]::ToByte($_, 16) }

            $TestPath = Join-Path $TestDrive -ChildPath 'Can-create-all-types-of-log-records.txt'
            [System.IO.File]::WriteAllBytes($TestPath, $Bytes)

            $Acctual = Import-ZosHulftLog -Path $TestPath

            $Acctual[0] | Should BeOfType "ZosHulftLog.EmptyLogRecord"
            $Acctual[1] | Should BeOfType "ZosHulftLog.DelimiterRecord"
            $Acctual[2] | Should BeOfType "ZosHulftLog.ObserveLogRecord"
            $Acctual[3] | Should BeOfType "ZosHulftLog.DelimiterRecord"
            $Acctual[4] | Should BeOfType "ZosHulftLog.JobExecutionLogRecord"
            $Acctual[5] | Should BeOfType "ZosHulftLog.DelimiterRecord"
            $Acctual[6] | Should BeOfType "ZosHulftLog.DelimiterRecord"
            $Acctual[7] | Should BeOfType "ZosHulftLog.DelimiterRecord"
            $Acctual[8] | Should BeOfType "ZosHulftLog.DelimiterRecord"
        }
    }
}
