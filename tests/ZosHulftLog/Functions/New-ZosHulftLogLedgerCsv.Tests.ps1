﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")

Describe "New-ZosHulftLogLedgerCsv" {
    Context "Unit test" {
        It "Writes header only when there is no send/receive log in input file" {
            $AHRec = "C1" + "00" * 168
            $CHRec = "C3" + "00" * 190
            $CDRec = "C340" + "00" * 524
            $CTRec = "C3" + "FF" * 190
            $JDRec = "D140" + "00" * 391
            $RHRec = "D9" + "00" * 190
            # $RDRec = "D940" + "00" * 3650
            $RTRec = "D9" + "FF" * 190
            $SHRec = "E2" + "00" * 190
            # $SDRec = "E240" + "00" * 3791
            $STRec = "E2" + "FF" * 190

            $Hex = @(
                $AHRec,$CHRec,$CDRec,$CTRec,$JDRec,$RHRec,$RTRec,$SHRec,$STRec
            ) -join ""
            $Bytes = 0..(($Hex.Length/2)-1) | %{$Hex.Substring($_*2,2)} | %{ [Convert]::ToByte($_, 16) }

            $HulftLogPath = Join-Path $TestDrive -ChildPath 'Returns-several-types-of-log-records.txt'
            [System.IO.File]::WriteAllBytes($HulftLogPath, $Bytes)

            $OutCsvPath = Join-Path $TestDrive -ChildPath 'Returns-several-types-of-log-records.csv'
            New-ZosHulftLogLedgerCsv -HulftLogPath $HulftLogPath -OutCsvPath $OutCsvPath

            $Acctual = Get-Content -Path $OutCsvPath -Encoding Default

            $Acctual | Should Be "LogType,Date,StartTime,EndTime,TransFileId,Host,FilePath,RecCnt,DataSize"
        }
    }
}
