﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\..\Import-TargetPackage.ps1")

Describe "KeyField::new" {
    Context "Constructor" {
        It "Can be created with empty values" {
            [ZosHulftLog.KeyField]::new(
                "A",
                "",
                "",
                [Datetime]::MinValue,
                [Datetime]::MinValue,
                [Datetime]::MinValue
            ) | Should Not BeNullOrEmpty
        }

        It "Can be created with real values" {
            @(
                [ZosHulftLog.KeyField]::new("A", "", "", [Datetime]::MinValue, [Datetime]::MinValue, [Datetime]::MinValue),
                [ZosHulftLog.KeyField]::new("R", "Receive-File-001", "Host-001", (Get-Date -Date "2021/08/31 07:31:11"), (Get-Date -Date "2021/08/31 07:31:25"), (Get-Date -Date "2021/08/31 07:31:25")),
                [ZosHulftLog.KeyField]::new("S", "Send-File-001", "Host-002", (Get-Date -Date "2021/08/31 07:31:15"), (Get-Date -Date "2021/08/31 07:31:20"), (Get-Date -Date "2021/08/31 07:31:20"))
            ) | Should Not BeNullOrEmpty
        }
    }
}

Describe "KeyField.LogClassification" {
    Context "Irregular" {
        It "Throws when unknown value is set" {
            {
                [ZosHulftLog.KeyField]::new(
                    "B",
                    "",
                    "",
                    [Datetime]::MinValue,
                    [Datetime]::MinValue,
                    [Datetime]::MinValue
                )
            } | Should Throw
        }
    }
}
