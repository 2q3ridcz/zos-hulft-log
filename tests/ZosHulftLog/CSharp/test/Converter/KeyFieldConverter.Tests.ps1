﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\..\Import-TargetPackage.ps1")

Describe "KeyFieldConverter.Convert" {
    Context "Returns KeyField Object" {
        It "Can be created with empty values" {
            $Bytes = @(
                "C3", "C6", "C9", "D3", "C5", "60", "C9", "C4", "00", "00"
                "00", "00", "00", "00", "00", "00", "00", "00", "00", "00"
                "00", "00", "00", "00", "00", "00", "00", "00", "00", "00"
                "00", "00", "00", "00", "00", "00", "00", "00", "00", "00"
                "00", "00", "00", "00", "00", "00", "00", "00", "00", "00"
                "00", "01", "32", "DF", "10", "00", "9F", "26", "B6", "C3"
                "C8", "D6", "E2", "E3", "6D", "D5", "C1", "D4", "C5", "00"
                "00", "00", "00", "00", "00", "00", "00", "00", "00", "00"
                "00", "00", "00", "00", "00", "00", "00", "00", "00", "00"
                "00", "00", "00", "00", "00", "00", "00", "00", "00", "00"
                "00", "00", "00", "00", "00", "00", "00", "00", "00", "00"
                "01", "32", "DF", "10", "00", "9F", "26", "B6", "C3", "01"
                "32", "DF", "10", "00", "9F", "26", "B6"
            ) |
            %{ [Convert]::ToByte($_, 16) }
            
            $Converter = [ZosHulftLog.Converter.KeyFieldConverter]::new()
            $Acctual = $Converter.Convert($Bytes)

            $Expect = "ZosHulftLog.KeyField"
            $Acctual | Should BeOfType $Expect
            $Acctual.LogClassification | Should Be "C"
            $Acctual.FileID | Should Be "FILE-ID"
            $Acctual.HostName | Should Be "HOST_NAME"
            $Acctual.RegisterationDateTime | Should Be ([DateTime]::Parse("2011/11/20 10:43:01.34"))
            $Acctual.RegisterationDateTimeSK2 | Should Be ([DateTime]::Parse("2011/11/20 10:43:01.34"))
            $Acctual.RegisterationDateTimeSK3 | Should Be ([DateTime]::Parse("2011/11/20 10:43:01.34"))
        }
    }
}