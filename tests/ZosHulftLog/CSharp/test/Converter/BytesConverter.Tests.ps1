﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\..\Import-TargetPackage.ps1")

Describe "BytesConverter.Convert" {
    Context "Unit test" {
        It "Returns EmptyLogRecord when A-start bytes are given" {
            $Hex = @("C1")
            0..167 | %{ $Hex += "00" }
            $Bytes = $Hex | %{ [Convert]::ToByte($_, 16) }
            $Acctual = [ZosHulftLog.Converter.BytesConverter]::Convert($Bytes)

            $Expect = "ZosHulftLog.EmptyLogRecord"
            $Acctual | Should BeOfType $Expect
        }

        It "Returns ObserveLogRecord when C-start bytes are given" {
            $Hex = @("C3", "40")
            0..523 | %{ $Hex += "00" }
            $Bytes = $Hex | %{ [Convert]::ToByte($_, 16) }
            $Acctual = [ZosHulftLog.Converter.BytesConverter]::Convert($Bytes)

            $Expect = "ZosHulftLog.ObserveLogRecord"
            $Acctual | Should BeOfType $Expect
        }

        It "Returns EmptyLogRecord and ObserveLogRecord when A-start and C-start bytes are given" {
            $Hex = @("C1")
            0..167 | %{ $Hex += "00" }
            $Hex += "C3"
            $Hex += "40"
            0..523 | %{ $Hex += "00" }
            $Bytes = $Hex | %{ [Convert]::ToByte($_, 16) }
            $Acctual = [ZosHulftLog.Converter.BytesConverter]::Convert($Bytes)

            $Expect = @("ZosHulftLog.EmptyLogRecord", "ZosHulftLog.ObserveLogRecord")
            $Acctual.Length | Should Be $Expect.Length
            $Acctual[0] | Should BeOfType $Expect[0]
            $Acctual[1] | Should BeOfType $Expect[1]
        }
    }
}
