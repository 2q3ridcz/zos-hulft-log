﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\..\Import-TargetPackage.ps1")

Describe "ByteArray.SubBytes" {
    Context "Unit test" {
        BeforeEach {
            $Bytes = @(
                "12", "34", "56", "78", "9A", "BC", "DE"
            ) |
            %{ [Convert]::ToByte($_, 16) }
        }

        It "Returns input when start = 0 and length = input length" {
            $Start = 0
            $Length = $Bytes.Length
            $Acctual = [ZosHulftLog.Converter.ByteArray]::SubBytes($Bytes, $Start, $Length)

            $Expect = $Bytes
            $Acctual | Should Be $Expect
        }

        It "Returns head parts of input when start = 0 and length < input length" {
            $Start = 0
            $Length = 2
            $Acctual = [ZosHulftLog.Converter.ByteArray]::SubBytes($Bytes, $Start, $Length)

            $Expect = @("12", "34") | %{ [Convert]::ToByte($_, 16) }
            $Acctual | Should Be $Expect
        }

        It "Returns mid parts of input when 0 < start and start + length < input length" {
            $Start = 2
            $Length = 3
            $Acctual = [ZosHulftLog.Converter.ByteArray]::SubBytes($Bytes, $Start, $Length)

            $Expect = @("56", "78", "9A") | %{ [Convert]::ToByte($_, 16) }
            $Acctual | Should Be $Expect
        }

        It "Returns tail parts of input when start + length = input length" {
            $Start = 3
            $Length = 4
            $Acctual = [ZosHulftLog.Converter.ByteArray]::SubBytes($Bytes, $Start, $Length)

            $Expect = @("78", "9A", "BC", "DE") | %{ [Convert]::ToByte($_, 16) }
            $Acctual | Should Be $Expect
        }

        It "Throws when length is less than 0" {
            $Start = 0
            $Length = -1
            { [ZosHulftLog.Converter.ByteArray]::SubBytes($Bytes, $Start, $Length) } | Should Throw
        }

        It "Returns null when length is 0" {
            $Start = 0
            $Length = 0
            $Acctual = [ZosHulftLog.Converter.ByteArray]::SubBytes($Bytes, $Start, $Length)
    
            $Acctual | Should BeNullOrEmpty
        }

        It "Throws when length is more than input length" {
            $Start = 0
            $Length = $Bytes.Length + 1
            { [ZosHulftLog.Converter.ByteArray]::SubBytes($Bytes, $Start, $Length) } | Should Throw
        }

        It "Throws when start is less than 0" {
            $Start = -1
            $Length = $Bytes.Length
            { [ZosHulftLog.Converter.ByteArray]::SubBytes($Bytes, $Start, $Length) } | Should Throw
        }

        It "Throws when start is larger or equal to input length" {
            $Start = $Bytes.Length
            $Length = 1
            { [ZosHulftLog.Converter.ByteArray]::SubBytes($Bytes, $Start, $Length) } | Should Throw
        }
    }
}

Describe "ByteArray.ToEBCIDICString" {
    Context "Unit test" {
        It "Returns decoded string" {
            $Bytes = @("C1", "C2", "C3", "00") | %{ [Convert]::ToByte($_, 16) }
            [ZosHulftLog.Converter.ByteArray]::ToEBCIDICString($Bytes) | Should Be "ABC"
        }
        It "Returns null when input is empty array" {
            [ZosHulftLog.Converter.ByteArray]::ToEBCIDICString(@()) | Should BeNullOrEmpty
        }
        It "Throws when input is null" {
            { [ZosHulftLog.Converter.ByteArray]::ToEBCIDICString($Null) } | Should Throw
        }
    }

    Context "Input: bytes, start, length" {
        It "Returns decoded string of input subbytes" {
            $Bytes = @("C1", "C2", "C3", "00") | %{ [Convert]::ToByte($_, 16) }
            [ZosHulftLog.Converter.ByteArray]::ToEBCIDICString($Bytes, 1, 3) | Should Be "BC"
        }
    }
}

Describe "ByteArray.PackedBCDToInt64" {
    Context "Unit test" {
        It "Returns positive number when input Hex ends with C" {
            $Bytes = @("12", "34", "56", "7C") | %{ [Convert]::ToByte($_, 16) }
            [ZosHulftLog.Converter.ByteArray]::PackedBCDToInt64($Bytes) | Should Be 1234567
        }
        It "Returns positive number when input Hex ends with F" {
            $Bytes = @("12", "34", "56", "7F") | %{ [Convert]::ToByte($_, 16) }
            [ZosHulftLog.Converter.ByteArray]::PackedBCDToInt64($Bytes) | Should Be 1234567
        }
        It "Returns negative number when input Hex ends with D" {
            $Bytes = @("12", "34", "56", "7D") | %{ [Convert]::ToByte($_, 16) }
            [ZosHulftLog.Converter.ByteArray]::PackedBCDToInt64($Bytes) | Should Be -1234567
        }

        It "Throws when input is null" {
            { [ZosHulftLog.Converter.ByteArray]::PackedBCDToInt64($Null) } | Should Throw
        }
    }

    Context "Input: bytes, start, length" {
        It "Returns decoded string of input subbytes" {
            $Bytes = @("C1", "C2", "C3", "00") | %{ [Convert]::ToByte($_, 16) }
            [ZosHulftLog.Converter.ByteArray]::ToEBCIDICString($Bytes, 1, 3) | Should Be "BC"
        }
    }
}

Describe "ByteArray.ToInt64" {
    Context "Input: bytes, start, length" {
        It "Returns int of input subbytes" {
            $Bytes = @("C1", "C2", "C3", "00") | %{ [Convert]::ToByte($_, 16) }
            [ZosHulftLog.Converter.ByteArray]::ToInt64($Bytes, 2, 2) | Should Be 49920
        }
    }
}

Describe "ByteArray.ToDateTime" {
    Context "Unit test" {
        It "Returns datetime" {
            $Bytes =
            @("01", "32", "DF", "10", "00", "9F", "26", "B6") |
            %{ [Convert]::ToByte($_, 16) }
            
            $Acctual = [ZosHulftLog.Converter.ByteArray]::ToDateTime($Bytes)
            $Expect = ([DateTime]::Parse("2011/11/20 10:43:01.34"))
            $Acctual | Should Be $Expect
        }

        It "Returns minvalue when All 00 is given" {
            $Bytes =
            @("00", "00", "00", "00", "00", "00", "00", "00") |
            %{ [Convert]::ToByte($_, 16) }
            
            $Acctual = [ZosHulftLog.Converter.ByteArray]::ToDateTime($Bytes)
            $Expect = ([DateTime]::MinValue)
            $Acctual | Should Be $Expect
        }

        It "Returns maxvalue when All FF is given" {
            $Bytes =
            @("FF", "FF", "FF", "FF", "FF", "FF", "FF", "FF") |
            %{ [Convert]::ToByte($_, 16) }
            
            $Acctual = [ZosHulftLog.Converter.ByteArray]::ToDateTime($Bytes)
            $Expect = ([DateTime]::MaxValue)
            $Acctual | Should Be $Expect
        }

        It "Throws when input is empty array" {
            { [ZosHulftLog.Converter.ByteArray]::ToDateTime(@()) } | Should Throw
        }
        
        It "Throws when input is null" {
            { [ZosHulftLog.Converter.ByteArray]::ToDateTime($Null) } | Should Throw
        }
        
        It "Throws when input length is less than 8" {
            $Bytes =
            @("01", "32", "DF", "10", "00", "9F", "26") |
            %{ [Convert]::ToByte($_, 16) }
            
            { [ZosHulftLog.Converter.ByteArray]::ToDateTime($Bytes) } | Should Throw
        }
    }

    Context "Input: bytes, start" {
        It "Returns datetime of input subbytes" {
            $Bytes =
            @("C1", "C2", "C3", "01", "32", "DF", "10", "00", "9F", "26", "B6", "C1", "C2", "C3") |
            %{ [Convert]::ToByte($_, 16) }

            $Acctual = [ZosHulftLog.Converter.ByteArray]::ToDateTime($Bytes, 3)
            $Expect = ([DateTime]::Parse("2011/11/20 10:43:01.34"))
            $Acctual | Should Be $Expect
        }
    }
}

Describe "ByteArray.PackedBCDToDatetime" {
    Context "Unit test" {
        It "Returns datetime" {
            $Bytes =
            @("02", "01", "11", "12", "0C", "01", "04", "30", "12", "1F") |
            %{ [Convert]::ToByte($_, 16) }
            
            $Acctual = [ZosHulftLog.Converter.ByteArray]::PackedBCDToDatetime($Bytes)
            $Expect = ([DateTime]::Parse("2011/11/20 10:43:01.21"))
            $Acctual | Should Be $Expect
        }

        It "Throws when input is empty array" {
            { [ZosHulftLog.Converter.ByteArray]::PackedBCDToDatetime(@()) } | Should Throw
        }
        
        It "Throws when input is null" {
            { [ZosHulftLog.Converter.ByteArray]::PackedBCDToDatetime($Null) } | Should Throw
        }
    }

    Context "Input: date_position, time_position" {
        It "Returns datetime of input subbytes" {
            $Bytes =
            @("EE", "02", "02", "50", "32", "2F", "EE", "EE", "00", "14", "65", "12", "3C", "EE") |
            %{ [Convert]::ToByte($_, 16) }

            $Acctual = [ZosHulftLog.Converter.ByteArray]::PackedBCDToDatetime($Bytes, 1, 8)
            $Expect = ([DateTime]::Parse("2025/03/22 01:46:51.23"))
            $Acctual | Should Be $Expect
        }
    }
}