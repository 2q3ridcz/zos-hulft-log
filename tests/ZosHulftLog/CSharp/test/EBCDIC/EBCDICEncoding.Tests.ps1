﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\..\Import-TargetPackage.ps1")

Describe "EBCDICEncoding::new" {
    Context "Unit test" {
        It "Can be created with empty values" {
            [EBCDIC.EBCDICEncoding]::new() | Should Not BeNullOrEmpty
        }
    }
}

Describe "EBCDICEncoding.GetBytes" {
    BeforeEach {
        $Encoding = [EBCDIC.EBCDICEncoding]::new()
    }

    Context "Unit test" {
        It "Returns null when null is given" {
            $Encoding.GetBytes($Null) | Should BeNullOrEmpty
        }

        It "Returns null when empty string is given" {
            $Encoding.GetBytes("") | Should BeNullOrEmpty
        }

        It "Converts string to bytes" {
            ### Common chars to all EBCDIC code pages. 
            ### https://ja.m.wikipedia.org/wiki/EBCDIC
            $String = " .<(+&*);-/,%_>?:#@'=`"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            $Acctual = $Encoding.GetBytes($String) | %{ $_.ToString("X2") }
            
            $Expect = @(
                "40", "4B", "4C", "4D", "4E",
                "50", "5C", "5D", "5E",
                "60", "61", "6B", "6C", "6D", "6E", "6F",
                "7A", "7B", "7C", "7D", "7E", "7F",
                "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9",
                "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9",
                "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9",
                "F0", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9"
            )
            $Acctual.Length | Should Be $Expect.Length

            for ($i = 0; $i -lt $Expect.Count; $i++) {
                $Acctual[$i] | Should Be $Expect[$i]
            }
        }
    }
}

Describe "EBCDICEncoding.GetString" {
    BeforeEach {
        $Encoding = [EBCDIC.EBCDICEncoding]::new()
    }

    Context "Unit test" {
        It "Throws when null is given" {
            { $Encoding.GetString($Null) } | Should Throw
        }

        It "Returns null when empty array is given" {
            $Encoding.GetString(@()) | Should BeNullOrEmpty
        }

        It "Returns empty string when array of null is given" {
            $Encoding.GetString(@($Null)) | Should Be ""
        }

        It "Converts bytes to string(Common chars)" {
            ### Common chars to all EBCDIC code pages. 
            ### https://ja.m.wikipedia.org/wiki/EBCDIC
            $Hexs = @(
                "40", "4B", "4C", "4D", "4E",
                "50", "5C", "5D", "5E",
                "60", "61", "6B", "6C", "6D", "6E", "6F",
                "7A", "7B", "7C", "7D", "7E", "7F",
                "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9",
                "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9",
                "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9",
                "F0", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9"
            )
            $Bytes = $Hexs | %{ [Convert]::ToByte($_, 16) }
            $Acctual = $Encoding.GetString($Bytes)

            $Expect = " .<(+&*);-/,%_>?:#@'=`"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            $Acctual | Should Be $Expect
        }

        It "Converts bytes to string" {
            $Bytes = 64..249 | %{ [Convert]::ToByte($_, 10) }
            $Acctual = $Encoding.GetString($Bytes)

            $Expect = " ｡｢｣､･ｦｧｨｩ£.<(+|&ｪｫｬｭｮｯ?ｰ?!¥*);¬-/abcdefgh?,%_>?[ijklmnop``:#@'=`"]ｱｲｳｴｵｶｷｸｹｺqｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉr?ﾊﾋﾌ~‾ﾍﾎﾏﾐﾑﾒﾓﾔﾕsﾖﾗﾘﾙ^¢\tuvwxyzﾚﾛﾜﾝﾞﾟ{ABCDEFGHI??????}JKLMNOPQR??????`$?STUVWXYZ??????0123456789"
            $Acctual | Should Be $Expect
        }
    }
}
