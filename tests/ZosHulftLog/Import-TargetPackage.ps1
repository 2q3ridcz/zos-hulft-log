$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\..\" | Resolve-Path

$PackageName = "ZosHulftLog"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"

Import-Module -Name $PackagePath -Force

$DllPath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\CSharp\build\$PackageName.dll"
[System.Reflection.Assembly]::LoadFile($DllPath) | Out-Null
