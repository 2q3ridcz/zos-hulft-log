$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path
$SrcFolderPath = $ProjectFolderPath | Join-Path -ChildPath "src"

Describe "PSScriptAnalyzer analysis" {
    $PSScriptAnalyzerExists = (Get-Module -ListAvailable PSScriptAnalyzer) -Ne $Null

    If ($PSScriptAnalyzerExists) {
        Import-Module -Name PSScriptAnalyzer -Force
        
        It "Should not return any violation" {
            Invoke-ScriptAnalyzer -Path $SrcFolderPath | Should BeNullOrEmpty
        }
    }
}
