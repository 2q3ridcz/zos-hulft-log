﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path
$SrcFolderPath = $ProjectFolderPath | Join-Path -ChildPath "src"
$TestsFolderPath = $ProjectFolderPath | Join-Path -ChildPath "tests"

Write-Host("Here  : " + $here)
Write-Host("Source: " + $SrcFolderPath)
Write-Host("Test  : " + $TestsFolderPath)

$Date = Get-Date

Get-Module -ListAvailable Pester, PSScriptAnalyzer

$TestResultFolderPath = "$here\Output" | Resolve-Path
$TestResultFileName = "TestResult.xml"
$TestResultFilePath = $TestResultFolderPath | Join-Path -ChildPath $TestResultFileName

# $CodeCoverageFileName = $Date.ToSTring("yyyyMMdd-HHmmss") + "_" + "CodeCoverage.xml"
# $CodeCoverageFilePath = $TestResultFolderPath | Join-Path -ChildPath $CodeCoverageFileName

$SrcFileFullNames =
Get-ChildItem -Path $SrcFolderPath -Recurse |
?{ @(".ps1", ".psm1") -contains $_.Extension } |
%{ $_.FullName }

$TestResult = Invoke-Pester `
    -PassThru `
    -Script $TestsFolderPath `
    -OutputFile $TestResultFilePath `
    -OutputFormat NUnitXML `
    -CodeCoverage $SrcFileFullNames
    # -CodeCoverageOutputFile $CodeCoverageFilePath

if($TestResult.FailedCount -gt 0) {
    ### Return to make GitLab CI job fail.
    EXIT 1
}
