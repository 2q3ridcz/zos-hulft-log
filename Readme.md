# zos-hulft-log

Reads log file of HULFT8 for zOS.


## Install

Run './src/ZosHulftLog/CSharp/build-script/build.ps1' to create dll. 

Dll will be placed at './src/ZosHulftLog/CSharp/build/ZosHulftLog.dll'.

## Usage

Check the sample usage in './console_logfile-to-csv' folder.


## About HULFT8 for zOS log file.

### Types and orders of records in log file

HULFT8 for zOS log file is consisted of several types of records in following order.

|Record type|Valid|First byte|Second byte|Record byte count|Number of records|
|--|--|--|--|--|--|
|Empty Log (Not mentioned in manual)|no|A|0x00|169|1|
|Observe Log (Header)|no|C|0x00|191|1|
|Observe Log|yes|C|Not (0x00 or 0xFF)|526|N|
|Observe Log (Trailer)|no|C|0xFF|191|1|
|Job Execution Log|yes|J|Not (0x00 or 0xFF)|393|N|
|Receive Log (Header)|no|R|0x00|191|1|
|Receive Log|yes|R|Not (0x00 or 0xFF)|3652|N|
|Receive Log (Trailer)|no|R|0xFF|191|1|
|Send Log (Header)|no|S|0x00|191|1|
|Send Log|yes|S|Not (0x00 or 0xFF)|3793|N|
|Send Log (Trailer)|no|S|0xFF|191|1|

First byte is the column named 'LogClassification'.

Second byte is part of a string column for valid log records, but 0x00 or 0xFF is set for invalid log records like empty, header, trailer record.

### Data size

In my case, ZosHulftLog binary file approximately is around 500 MB and contains 210,000 records.

## Reference

- HULFT8 for zOS log file format
    - [Official | Log file formats](https://www.hulft.com/help/en-us/HULFT-V8/ZOS-OPE/Content/HULFT_OPE_ZOS/LogFileFormat/LogFFrmt.htm)
    - [【公式】履歴ファイルのフォーマット](https://www.hulft.com/help/ja-jp/HULFT-V8/ZOS-OPE/Content/HULFT_OPE_ZOS/LogFileFormat/LogFFrmt.htm)
- EBCDIC
    - [EBCDIC - Wikipedia](https://ja.m.wikipedia.org/wiki/EBCDIC)
    - [Encoding クラス (System.Text) | Microsoft Docs](https://docs.microsoft.com/ja-jp/dotnet/api/system.text.encoding)
- PackedBCD
    - [IBM 資料 - IBM Documentation](https://www.ibm.com/docs/ja/i/7.1?topic=ssw_ibm_i_71/rzasd/sc092508276.htm)

## Note

Made for windows 10 environment with no additional installs allowed.

For this reason, Powershell is mainly used, but uses C# where speed is needed.
